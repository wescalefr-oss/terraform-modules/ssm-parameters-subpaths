This project is archived, activity is being migrated to https://gitlab.com/wescalefr-oss/terraform-modules/terraform-aws-ssm-parameters-subpaths for renaming purpose.       
Sources are kept for active stacks which would reference it. Please update your paths.

# SSM Parameters by path module

This module provides a program to query a specified path in SSM parameter stores and retrieve as a map the found parameters names keyed by their names relative to the queried path    
It uses Terraform >= 0.12 for structure.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| external | n/a |
| local | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| path | The SSM parameters path to query to retrieve the parameters from | `string` | n/a | yes |
| profile | An AWS profile to use in the SSM parameters query (leave empty for no profile) | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| names | Map of the name-keyed paths found by the SSM parameters query |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
